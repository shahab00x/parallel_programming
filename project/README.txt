FINAL PROJECT
SHAHAB ESLAMIAN
===========================================
=====================
In order to change the number of elements, you have
to change it from inside the code. Then run make again.
output.txt is a sample output file for 128 numbers.

The standard odd-even sort can be found inside the odd_even_sort folder.
The sample output file is output-odd-even.txt. To change the number of
elements you have to open h-job-icc.64 and change the last number.

The running times of these two programs can be found in times.txt
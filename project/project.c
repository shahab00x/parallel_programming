#include <stdio.h>
#include <mpi.h>
#include <time.h>
#include <stdlib.h>

// The size of the array
#define SIZE 128
#define NCPUS 8

double time_spent;

int k, l, temp;

int compare(const void*, const void*);
int* compare_split(int*, int*, int*, int);
int* swap(int*, int*, int);
int is_sorted(int*, int);
void Merge_split_high(int local_A[], int temp_B[], int temp_C[], int local_n);
void Merge_split_low(int local_A[], int temp_B[], int temp_C[], int local_n);
main(int argc, char** argv)
{
	int npes; // the number of processors
    int node;  // The rank of the calling process
	npes = NCPUS;	
	int array_1[SIZE];
	int local_array[SIZE/8];
	int nlocal = SIZE/8;
	int done;
	int d;
	int i = 0;
	
	for (i = 0; ;i++)
	  if ( (int)pow(2, i) >= NCPUS ){
		d = i;
		break;
	}
	srand(time(0));

    MPI_Init(&argc, &argv);
 // MPI_Ccmm_size(MPI_COMM_WORLD, &npes);
	MPI_Comm_rank(MPI_COMM_WORLD, &node);
		
	// Allocate separate iterators for each cpu
	if ( node == 0){
		for ( i = 0 ; i < SIZE; i++){
			array_1[i] = rand() % (SIZE+1); // [0, 128] inclusive
		}
 	
		int j[NCPUS];
		for (i = 0; i < NCPUS; i++)
			j[i] = 0;
	
    }
	MPI_Scatter(array_1, nlocal, MPI_INT, local_array, nlocal, MPI_INT, 0, MPI_COMM_WORLD);
	
	
	
	// Main algorithm
	int * c;
	c = (int*)malloc(nlocal * 2 * sizeof(int));
	int m,m2;
	int startend[2];
	int *buff = (int*)malloc(nlocal*sizeof(int));
	MPI_Status status;
	int dest, incr,p, x, count, it, bb, y;
	int start_end_array[2*NCPUS];
	x = 0;
	count = 0;
	done = 0;
	it = 0;



	time_spent = MPI_Wtime();
	qsort(local_array, nlocal, sizeof(int), compare);

	// PHASE I
	for (m2 = d-1; m2 >=0 ; m2--){
			p = (int)pow(2,m2);
			dest = node ^ p;
			if ( node & p){
				MPI_Send(local_array, nlocal, MPI_INT, dest, 3 , MPI_COMM_WORLD);
				MPI_Recv(buff, nlocal, MPI_INT, dest,5 , MPI_COMM_WORLD, &status);
				Merge_split_high(local_array, buff, c, nlocal);		
			}
			else{
				MPI_Send(local_array, nlocal, MPI_INT, dest, 5, MPI_COMM_WORLD);
				MPI_Recv(buff, nlocal, MPI_INT, dest, 3, MPI_COMM_WORLD, &status);
				Merge_split_low(local_array, buff, c, nlocal);
			}
			
	}
	
	// PHASE II
	while(!done){
		count++;
		// Perform odd-even iterations
	
		it = (it++);
		if( it % 2 != 0)
			if(node % 2 != 0 ){
			  if(node < NCPUS-1){
				MPI_Send(local_array, nlocal, MPI_INT, node+1, 6, MPI_COMM_WORLD);
				MPI_Recv(buff, nlocal, MPI_INT, node+1, 7, MPI_COMM_WORLD,&status);
				Merge_split_low(local_array, buff, c, nlocal);
			}
		}

		else {
			   if ( node > 0 ){
				MPI_Send(local_array, nlocal, MPI_INT, node-1, 7, MPI_COMM_WORLD);
				MPI_Recv(buff, nlocal, MPI_INT, node-1, 6, MPI_COMM_WORLD, &status);
				Merge_split_high(local_array, buff, c, nlocal);
		   	}
		}
		if ( it % 2 == 0)
		   if (node %2 == 0 ){
		    if(node < NCPUS-1){
			MPI_Send(local_array, nlocal, MPI_INT, node+1, 8, MPI_COMM_WORLD);
			MPI_Recv(buff, nlocal, MPI_INT, node+1, 9, MPI_COMM_WORLD, &status);
			Merge_split_low(local_array, buff, c, nlocal);
			}}
		    else if( node > 0){
			MPI_Send(local_array, nlocal, MPI_INT, node-1, 9, MPI_COMM_WORLD);
			MPI_Recv(buff, nlocal, MPI_INT, node-1, 8, MPI_COMM_WORLD, &status);
			Merge_split_high(local_array, buff, c, nlocal);
		}
		
		// If it is sorted done=1
		if (!node){ // node == 0
			start_end_array[0] = local_array[0];
			start_end_array[1] = local_array[nlocal-1];
			for (bb = 1; bb< NCPUS; bb++){
				MPI_Recv(startend, 2, MPI_INT, bb, 12, MPI_COMM_WORLD, &status);
				start_end_array[bb*2] = startend[0];
				start_end_array[bb*2+1] = startend[1];
				}
			x = is_sorted(start_end_array, 2*NCPUS);
		} else {
			startend[0] = local_array[0];
			startend[1] = local_array[nlocal-1];
			MPI_Send(startend, 2, MPI_INT, 0, 12, MPI_COMM_WORLD);
		}
		
		// broadcast
		MPI_Bcast(&x, 1, MPI_INT, 0, MPI_COMM_WORLD);
		if(x==1 || it >= NCPUS -1 ){done = 1;break;}
		else done = 0;

	}

	time_spent = MPI_Wtime() - time_spent;
	printf("After loop %d\n",count);
	fflush(stdout);
	
	MPI_Barrier(MPI_COMM_WORLD);
	
	sleep(1); // create a delay of 1 after the sorting is done

	MPI_Gather(local_array, nlocal, MPI_INT, array_1, nlocal, MPI_INT, 0, MPI_COMM_WORLD);
	// print final results        
	
	if (node == 0){
		printf("The final Result:\n");
		for ( i=0; i < SIZE; i++)
			printf("%d, ", array_1[i]);
		printf("\n\nTime Spent = %f\n\n",time_spent);
	}
        MPI_Finalize();
	
		
}

int compare(const void * a, const void *b){
	return (*(int*)a - *(int*)b);
}


int is_sorted(int* array, int size){
	for (k =0; k< size-1; k++)
		if ( array[k] > array[k+1]) 
			return -1;
	return 1;

}

void Merge_split_low(int local_A[], int temp_B[], int temp_C[], 
        int local_n) {
   int ai, bi, ci;
   
   ai = 0;
   bi = 0;
   ci = 0;
   while (ci < local_n) {
      if (local_A[ai] <= temp_B[bi]) {
         temp_C[ci] = local_A[ai];
         ci++; ai++;
      } else {
         temp_C[ci] = temp_B[bi];
         ci++; bi++;
      }
   }

   memcpy(local_A, temp_C, local_n*sizeof(int));
}  /* Merge_split_low */


void Merge_split_high(int local_A[], int temp_B[], int temp_C[], 
        int local_n) {
   int ai, bi, ci;
   
   ai = local_n-1;
   bi = local_n-1;
   ci = local_n-1;
   while (ci >= 0) {
      if (local_A[ai] >= temp_B[bi]) {
         temp_C[ci] = local_A[ai];
         ci--; ai--;
      } else {
         temp_C[ci] = temp_B[bi];
         ci--; bi--;
      }
   }

   memcpy(local_A, temp_C, local_n*sizeof(int));
}  /* Merge_split_high */


